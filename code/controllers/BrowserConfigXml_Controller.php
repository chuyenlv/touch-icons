<?php
class BrowserConfigXml_Controller extends Controller {

	private static $allowed_actions = array('index');

  public function index(SS_HTTPRequest $request) {

	  $fileSizes = array(
		  array(
			  'width' => 70,
			  'height' => 70,
			  'type' => 'square'
		  ),
		  array(
			  'width' => 150,
			  'height' => 150,
			  'type' => 'square'
		  ),
		  array(
			  'width' => 310,
			  'height' => 310,
			  'type' => 'square'
		  ),
		  array(
			  'width' => 310,
			  'height' => 150,
			  'type' => 'wide'
		  )
	  );

	  $siteConfig = SiteConfig::current_site_config();
	  $list = ArrayList::create();
	  foreach ($fileSizes as $fileSize) {

		  switch ($fileSize['type']) {
			  case 'square':
				  $image = $siteConfig->TouchIcon()->croppedImage($fileSize['width'], $fileSize['height']);
				  break;

			  case 'wide':
				  $image = $siteConfig->TouchIcon()->PaddedImage($fileSize['width'], $fileSize['height']);
				  break;
		  }

		  $filename = $image->Filename;

		  $list->push(array(
				'Width' => $fileSize['width'],
				'Height' => $fileSize['height'],
			  'Type' => $fileSize['type'],
			  'Source' => $filename
		  ));
	  }

	  $tileColour = $siteConfig->Windows8TileColour?$siteConfig->Windows8TileColour:'ffffff';
	  return $this->customise(array(
	    'ImagesList' => $list,
		  'TileColour' => $tileColour
	  ))->renderWith(array(
		  'BrowserConfigXml'
	  ));

  }
}
