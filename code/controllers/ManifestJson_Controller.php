<?php
class ManifestJson_Controller extends Controller {

	private static $allowed_actions = array('index');

  public function index(SS_HTTPRequest $request) {

	  $fileSizes = array(
		  array(
			  'size' => 36,
			  'density' => '0.75'
		  ),
		  array(
			  'size' => 48,
			  'density' => '1.0'
		  ),
		  array(
			  'size' => 72,
			  'density' => '1.5'
		  ),
		  array(
			  'size' => 96,
			  'density' => '2.0'
		  ),
		  array(
			  'size' => 144,
			  'density' => '3.0'
		  ),
		  array(
			  'size' => 192,
			  'density' => '4.0'
		  )
	  );

	  $siteConfig = SiteConfig::current_site_config();
	  $list = ArrayList::create();
	  foreach ($fileSizes as $fileSize) {

		  $image = $siteConfig->TouchIcon()->croppedImage($fileSize['size'], $fileSize['size']);
		  $filename = str_replace('/', '\\/', $image->Filename);

		  $list->push(array(
				'Source' => '\\/' . $filename,
			  'Sizes' => sprintf('%dx%d',
				  $fileSize['size'],
				  $fileSize['size']
			  ),
			  'Type' => 'image\\/png',
			  'Density' => $fileSize['density']
		  ));
	  }
	  return $this->customise(array(
	    'ImagesList' => $list
	  ))->renderWith(array(
		  'ManifestJson'
	  ));

  }
}
