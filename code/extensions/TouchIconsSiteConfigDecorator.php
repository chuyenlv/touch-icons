<?php
class TouchIconsSiteConfigDecorator extends DataExtension {

	private static $db = array(
		'Windows8TileColour' => 'Varchar(6)'
	);

	private static $has_one = array(
		'TouchIcon' => 'Image',
		'FavIcon' => 'Image'
	);

	public function updateCMSFields(FieldList $fields) {

		$fields->addFieldToTab('Root.TouchIcons', UploadField::create('TouchIcon', 'Touch icon image')
				->setFolderName('touch-icons')
				->setRightTitle('A square png image which needs to be at least 192x192')
		);

		$fields->addFieldToTab('Root.TouchIcons', UploadField::create('FavIcon', 'Fav icon image')
				->setFolderName('touch-icons')
				->setRightTitle('A square png image which needs to be at least 32x32')
		);
	}

	function __construct() {
		parent::__construct();
	}

	function onAfterWrite() {
		parent::onAfterWrite();

		$favicon = $this->owner->FavIcon();
		if ($favicon && $favicon->ID > 0) {


			$source = sprintf('%s/%s',
				BASE_PATH,
				$favicon->Filename
			);
			$destination = ASSETS_PATH . '/touch-icons/favicon.ico';

			if (file_exists($destination)) {
				unlink($destination);
			}

			$ico_lib = new PHP_ICO( $source );
			$ico_lib->save_ico( $destination );


		}


	}

}





