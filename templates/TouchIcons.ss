<% if $SiteConfig.TouchIcon %>
  <link rel="apple-touch-icon" sizes="57x57" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(57,57).Filename}">
  <link rel="apple-touch-icon" sizes="60x60" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(60,60).Filename}">
  <link rel="apple-touch-icon" sizes="72x72" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(72,72).Filename}">
  <link rel="apple-touch-icon" sizes="76x76" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(76,76).Filename}">
  <link rel="apple-touch-icon" sizes="114x114" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(114,114).Filename}">
  <link rel="apple-touch-icon" sizes="120x120" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(120,120).Filename}">
  <link rel="apple-touch-icon" sizes="144x144" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(144,144).Filename}">
  <link rel="apple-touch-icon" sizes="152x152" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(152,152).Filename}">
  <link rel="apple-touch-icon" sizes="180x180" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(180,180).Filename}">
<% end_if %>
<% if $SiteConfig.FavIcon %>
  <link rel="icon" type="image/png" href="{$BaseHref}{$SiteConfig.FavIcon.CroppedImage(32,32).Filename}" sizes="32x32">
  <link rel="shortcut icon" href="assets/touch-icons/favicon.ico">
<% end_if %>
<% if $SiteConfig.TouchIcon %>
  <link rel="icon" type="image/png" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(192,192).Filename}" sizes="192x192">
  <link rel="icon" type="image/png" href="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(96,96).Filename}" sizes="96x96">
<% end_if %>

<link rel="manifest" href="touchicons/manifest.json">

<meta name="msapplication-TileColor" content="#ffffff">
<% if $SiteConfig.TouchIcon %>
  <meta name="msapplication-TileImage" content="{$BaseHref}{$SiteConfig.TouchIcon.CroppedImage(144,144).Filename">
<% end_if %>
<meta name="msapplication-config" content="touchicons/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
